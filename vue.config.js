const StyleLintPlugin = require("stylelint-webpack-plugin");
const webpack = require("webpack");

module.exports = {
  configureWebpack: {
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery",
        jquery: "jquery",
        "window.jQuery": "jquery",
        jQuery: "jquery",
        Popper: ["popper.js", "default"]
        // In case you imported plugins individually, you must also require them here:
        // Util: "exports-loader?Util!bootstrap/js/dist/util",
        // Dropdown: "exports-loader?Dropdown!bootstrap/js/dist/dropdown",
      }),
      new StyleLintPlugin({
        files: ["src/**/*.{vue,scss}"]
      })
    ]
  },
  /*
  css: {
    loaderOptions: {
      sass: {
        prependData: '@import "~@/assets/styles/_main.scss";'
      }
    }
  }
  */ pluginOptions: {
    i18n: {
      locale: "sv",
      fallbackLocale: "en",
      localeDir: "lang",
      enableInSFC: false
    }
  }
};
